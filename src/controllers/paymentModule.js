const stripe = require('stripe')('sk_test_51GwkIdAnl0psCHWwEZWAGZ6LUHHSns4DJADaU8BlwNT4XfLWBzeeWT8BCzIgRInaEMLEj5AJ08qBjvdittPyiWca00giNkfrXs');

const stripeSecretKey=process.env.SECRET_KEY;
const stripePublicKey=process.env.STRIPE_PUBLIC_KEY;

exports.paymentStripe =  (req, res, next) => {
    return stripe.customers.create({
        email: 'faten.ghandour.dsoservices@gmail.com',
        source: 'tok_mastercard'
    })
        .then(customer => {
            stripe.charges.create({
                amount: req.body.amount, // Unit: cents
                currency: 'eur',
                customer: customer.id,
                description: 'Test payment',
            })
        }).then(charge => res.json({message:"success",status:'200'}))
        .catch(function(error) {
            res.status(403).send({message:"error",status:'400'})
        });
}
