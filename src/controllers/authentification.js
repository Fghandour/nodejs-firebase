import {adminFirebase,firebaseClient} from '../services/admin'
import {sendNotification,addDeviceByUserId,getDevicesByUserId} from './notification'
const shortid = require('shortid');
var events = require('events');
var progressEmitter = new events.EventEmitter();
const db = adminFirebase.database();
const usersRef = db.ref("users");
exports.createUser =  (req, res) => {
        const {
            email,
            password,
            firstName,
            lastName,
            phoneNumber,
            photoUrl
        } = req.body;
    const newUser={
             email: email,
             emailVerified: false,
             password: password,
             displayName: `${firstName} ${lastName}`,
             disabled: false,
            phoneNumber:phoneNumber,
         }

    adminFirebase.auth().createUser(newUser)
        .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            var user={userId:userRecord.email,code:shortid.generate()}
            usersRef.push(user,function(err){
                if(err){
                    return  res.json({message:"Unauthorized",status:'401'})
                }else{
                    return  res.json({message:"Success",status:'200',"user": userRecord.uid})
                }
            })
        })
        .catch(function(error) {
            res.status(403).send({"error": error})
        });

};
exports.checkIfAuthenticated =  (req, res, next) => {
    const {email,password,appType,TotalPrice,products}=req.body;
    firebaseClient.auth().signInWithEmailAndPassword(email,password)
        .then(async function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            if(appType=='web'){

                var data=await getDevicesByUserId(email);
                data.forEach(element => {
                    var notification={
                        'token':element,
                        'notification':{
                            "body": "Notification Test  body!",
                            "title": "Notification Test",
                            "icon": "appicon"
                        },
                        'data':{
                            "name" : "Giddy",
                            "price" :''+TotalPrice+'',
                            "products":products,

                        }
                    }
                    sendNotification(notification)

                })
            }



            return  res.json({"user":userRecord.user,status:'200'})
        })
        .catch(function(error) {
            return  res.json({"user":null,status:'400'})

        });
};
exports.verifCode =  (req, res, next) => {
    const {code}=req.body;
     usersRef.orderByChild("code").equalTo(code).on("value",function(snapshot) {
       if (snapshot.exists()){
            return  res.json({validate:true,status:'200'})

        }else{
            return  res.json({validate:false,status:'200'})

        }
    })
}


exports.logout =  (req, res, next) => {
    firebaseClient.auth().signOut().then(() => {
        return  res.json({"message":'success',status:'200'})
    }, function (error) {
        return  res.json({"message":'error',status:'400'})
    });
}

exports.sendShopNotification = async (req, res, next) => {
    const {email,TotalPrice,products}=req.body;
    var data=await getDevicesByUserId(email);
    data.forEach(element => {
        var notification = {
            'token': element,
            'notification': {
                "body": "Notification Test  body!",
                "title": "Notification Test",
                "icon": "appicon"
            },
            'data': {
                "name": "Giddy",
                "price": '' + TotalPrice + '',
                "products": products,

            }
        }
        sendNotification(notification);
        return  res.json({"message":'success',status:'200'})

    })
};
