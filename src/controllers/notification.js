import {adminFirebase,firebaseClient} from '../services/admin'
const db = adminFirebase.database();
const deviceRef = db.ref("Devices");
export function sendNotification(req) {
        var data=""
    const notification_options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };
    const  registrationToken = req.token;
    const options =  notification_options
    const payload = {
        'notification': req.notification,
        // NOTE: The 'data' object is inside payload, not inside notification
        'data':  req.data
    };

    adminFirebase.messaging().sendToDevice(registrationToken, payload, options)
        .then( response => {
            data='Notification sent successfully'

        })
        .catch( error => {
            data=error

        });
};
//  exemple of request
// {
//     "notification":{
//     "title": "Notification Title",
//         "body": "This is an example notification"
// }
// }
exports.addDeviceByUserId = async (req, res, next) => {
    const {userId,devicesToken}=req.body
    if(!userId || !devicesToken){
        return  res.json({message:"Bad Request",status:'400'})
    }

    var {exist} = await checkExistDevice(userId,devicesToken);
    if(exist){
        return res.json({message:"Exist Device",status:'400'})
    }
    var device={userId:userId,deviceToken:devicesToken}
    deviceRef.push(device,function(err){
        if(err){
            return  res.json({message:"Unauthorized",status:'400'})
        }else{
            return  res.json({message:"success",status:'200'})
        }
    })
};

async function checkExistDevice(userId,deviceToken){
    var data ={exist:false,device:null};
    await deviceRef.orderByChild("deviceToken").equalTo(deviceToken).once("value",snapshot => {
        if (snapshot.exists()){
            snapshot.forEach(function (childSnapshot) {
                if(childSnapshot.val().userId ==userId) {
                    data.exist=true;
                }
            });
        }
    });
    return data;
}
export async function getDevicesByUserId (userEmail){
    var {devices} = await getDevicesByUserEmail(userEmail);
        return devices;

}

 async function getDevicesByUserEmail(userEmail){
    var data ={exist:false,devices:[]};
    await deviceRef.orderByChild("userId").equalTo(userEmail).once("value",snapshot => {
        if (snapshot.exists()){
            data.exist=true;
            snapshot.forEach(function (childSnapshot) {
                data.devices.push(childSnapshot.val().deviceToken);
            });

        }
    });
    return data;
}

