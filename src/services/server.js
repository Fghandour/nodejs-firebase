import express from "express";
import bodyParser from "body-parser";
const server = express();
var multer = require('multer');
const cors = require('cors'); // addition we make
var upload = multer();
import {createUser,checkIfAuthenticated,verifCode,logout,sendShopNotification} from '../controllers/authentification'
import {sendNotification,addDeviceByUserId,getDevicesByUserId} from '../controllers/notification';
import {paymentStripe} from '../controllers/paymentModule'

server.use(express.json());
server.use(cors());

// for parsing application/x-www-form-urlencoded
server.use(express.urlencoded({ extended: true }));

// for parsing multipart/form-data
server.use(upload.array());
server.use(express.static('public'));
server.post('/user/signup', [createUser]);
server.post('/user/signin',[checkIfAuthenticated]);
server.post('/user/addDevice',[addDeviceByUserId]);
server.post('/send/notification', [sendNotification])
server.post('/user/getDevices',[getDevicesByUserId]);
server.post('/payment',[paymentStripe]);
server.post('/user/verifCode',[verifCode]);
server.post('/user/logout',[logout]);
server.post('/notification',[sendShopNotification]);
const io = require('socket.io')();

io.on('connection', (client) => {
    client.on('clientConnected', (valid) => {
        console.log('client is connecting ', valid);
        client.broadcast.emit('connected', valid);

    });
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);


export default server;
