
var admin = require("firebase-admin");
const firebase = require("firebase/app");
require("firebase/auth");

var serviceAccount = require("./fir-api-a582b-firebase-adminsdk-8v2uc-8ff49b9ed3");

export const adminFirebase= admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fir-api-a582b.firebaseio.com"
});
export const firebaseClient = firebase.initializeApp({
    apiKey: "AIzaSyA94fHQTuEVvlLz1bLMooenxNeKmj5Nx1c"
});
